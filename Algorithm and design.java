Algorithm and design
 


Q1.  Implementation: 

A. Use the sorting algorithm you implemented in worksheet 1 (Selection Sort or Bubble Sort) and 
modify it to operate on a doubly linked list implemented using three arrays.  Prev[] , Next[] , Key[] .  Populate your linked list and print it out. Run your sorting algorithm on it and print out the resulting linked list (note when you print out the linked list you should print out each array underneath one 

another eg:  

   P[0,1,2,3,4,5,6] 

   K[10,11,12,13,14,15] 

   N[1,2,3,4,5,6,0] 

null should be represented by a backslash or forward slash.  Be sure to include your pseudocode and design elements in your .java files. Methods and calls are up to you to decide on (but your 
pseudocode should show what your code performs) 

 

B.  

You are asked by a client to design, describe and implement a program that asks a user how many values they want in an array. 

Generate that many whole numbers between 1 and 1000 (integers, no decimal places, use any 
random number function built into java) and populate an array. perform a mergesort , an insertion sort and a quicksort on the array and report the time taken for each sorting algorithm as well as how many times it has to swap an element in the array before it is finished. (you can use any code, even code you find online as long as you credit and comment it for the sorting algorithms) 




 

 