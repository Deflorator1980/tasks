Александр
SEM- .NET (chat system)

Preconditions
You should work on your local machine.
You may use Visual Studio 2010 to 2013.
You may use SQL Server 2008 to 2014 (Main or Express edition).
Requirements
Overall Objective

Create the architecture and design of a real time chat system. Implement the system's services and applications.

Functional Specifications

The system allows its users to chat with other users in "real time". The system must meet the following specifications:

Create a web application that:
allows a user to
register, log in and log out
see who is online
chat one to one with another user
send and display emoticons (2-3 types of icons only)
be logged out after 5 minutes of inactivity
uses the service below to perform all its operations
Create a service that:
uses a database to persist user log ins
does not persist chat sessions or history
provides all operations needed to support "real time" chatting
Technical Specifications

The following list of technical specifications should be adhered to:

For the service
Create a WCF service for the service component
Secure the service
Use Entity Framework for database access
Should support "real time" (asynchronous) chatting without "polling"
For the application
Create an ASP.NET web forms or MVC4+ web application
Should support real time chatting using a framework like ASP.NET SignalR (instead of regular page refresh)
Does not uses any database
Apply input validations and constraints wherever necessary to create a stable service and application.
Even if you are not able to complete all the tasks, try to achieve a working system.
Add missing requirements to the implementation, according to your experience
Deliverables
Application Demo

Record the video demonstration of the web application using Wink (or any other tool) intermixed with the execution of service and client. Save the video to your local machine, and include it in the delivery package.

Database script

Create manual steps and/or SQL script files to create the databases, their schema, stored procedures and any initial data you may use for testing.

Readme Document

Create a txt file with the following information

Steps to install and configure any prerequisites for the development environment
Steps to create and initialize the databases
Steps to prepare the source code to build properly
Any assumptions made and missing requirements that are not covered in the requirements
Any feedback you wish to give about improving the assignment
Design Document

A doc file containing the following information and diagrams

List of technologies and patterns used
Explanation of the architecture/design implemented and the reasons of various choices
Any diagrams including a component interaction, activity, sequence or class diagrams of the important components.
To be evaluated
The quality of the functional output and code
The architecture, design quality and completeness
Technologies and design applied
Extra validations and assumptions which are not described
Proper documentation and demonstration video
Delivery / What to submit
Please, read and follow this section carefully. Any delivery that does not follow this section will score much less or simply won't be evaluated.

Delivery for this assignment should consist of an archive named <your_name> - SEM.NET - Chat.zip containing the following

Source code/project including SQL scripts
Wink recording, download version 2 from Wink, render the video to swf format
Readme.txt containing the instructions to configure and run the application, notes and feedback
Design.doc with needed diagrams
Structure of the resulting zip file should be of the following format

<your_name> - SEM.NET - Chat.zip
<your_name> - SEM.NET - Chat.zip \Readme.txt
<your_name> - SEM.NET - Chat.zip \Design.doc
<your_name> - SEM.NET - Chat.zip \Wink\ «< this folder should contain the wink recording
<your_name>
- SEM.NET - Chat.zip \Source\ «< this folder should contain the complete source code for the project(s)